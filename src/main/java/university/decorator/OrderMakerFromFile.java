package university.decorator;

import java.io.*;

public class OrderMakerFromFile implements Ingestion {
    private String name;

    public OrderMakerFromFile(String name) {
        this.name = name;
    }

    @Override
    public void writeOrder(String order) {
        File file = new File(name);
        try (OutputStream fos = new FileOutputStream(file)) {
            fos.write(order.getBytes(), 0, order.length());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }}

    @Override
    public String readOrder() {
        char[] buffer = null;
        File file = new File(name);
        try (FileReader reader = new FileReader(file)) {
            buffer = new char[(int) file.length()];
            reader.read(buffer);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return new String(buffer);
    }
}

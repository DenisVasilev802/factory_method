package university.decorator;

import java.util.Locale;

/**
 * Легенда:
 * Человек сделал заказ с помощью зеркала как Леонардо да Винчи.
 * (Будем считать что нужно просто поменять буквы с большой на маленькую)
 */
public class EncryptUsingMirror extends OrderDecorator {

    public EncryptUsingMirror(Ingestion wrapper) {
        super(wrapper);
    }
    @Override
    public void writeOrder(String data) {
        super.writeOrder(encode(data));
    }

    @Override
    public String readOrder() {
        return decode(super.readOrder());
    }
    private String encode(String data) {
        return data.toLowerCase(Locale.ROOT);
    }

    private String decode(String data) {

        return data.toUpperCase(Locale.ROOT);
    }
}

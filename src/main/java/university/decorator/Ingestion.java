package university.decorator;

/**
 * Легенда:
 * Ресторан, прием пищи. Люди сами формируют заказы могут написать на китайском или вообще с помощью зеркала.
 */
public interface Ingestion {

    void writeOrder(String order);
    String readOrder();
}

package university.decorator;

public class OrderDecorator implements Ingestion {
private Ingestion wrapper;

    public OrderDecorator(Ingestion wrapper) {
        this.wrapper = wrapper;
    }

    @Override
    public void writeOrder(String order) {
        wrapper.writeOrder(order);
    }

    @Override
    public String readOrder() {
        return wrapper.readOrder();
    }
}


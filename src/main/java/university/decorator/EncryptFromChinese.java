package university.decorator;

public class EncryptFromChinese extends OrderDecorator {

    public EncryptFromChinese(Ingestion wrapper) {
        super(wrapper);
    }
    @Override
    public void writeOrder(String data) {
        super.writeOrder(encode(data));
    }

    @Override
    public String readOrder() {
        return decode(super.readOrder());
    }
    private String encode(String data) {
        return data+2;
    }

    private String decode(String data) {

        return data+1;
    }
}

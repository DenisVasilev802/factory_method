package university;

import university.chainResponsibility.Bank;
import university.chainResponsibility.BankConnection;
import university.chainResponsibility.EnterPassword;
import university.chainResponsibility.Payment;
import university.decorator.EncryptFromChinese;
import university.decorator.EncryptUsingMirror;
import university.decorator.OrderDecorator;
import university.decorator.OrderMakerFromFile;
import university.factory_method.Factory.ISortFactory;
import university.factory_method.Factory.SelectionSortMaker;
import university.factory_method.GenerateRandomArray;
import university.flyweight.Hair;
import university.observer.Channel;
import university.observer.SmsNotificationListener;
import university.prototype.Sheep;
import university.prototype.SheepDolly;

import java.awt.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int randomFrom = 0;
        int randomTo = 100;
        int size = 10;
        GenerateRandomArray generateRandomArray = new GenerateRandomArray();

        ISortFactory sortFactory = new SelectionSortMaker();
        System.out.println(Arrays.toString(sortFactory.makeSortMethod().sort(generateRandomArray.generate(randomFrom, randomTo, size))));

        Sheep sheep1 = new SheepDolly(15, 2, 25, "White");
        Sheep sheep2 = sheep1.clone();
        System.out.println(sheep2.toString());
        OrderDecorator order = new EncryptFromChinese(new EncryptUsingMirror(new OrderMakerFromFile("text.txt")));
        Hair hair = new Hair();
        for (int i = 0; i < 1000; i++) {
            hair.addFilament(random(0, 500), random(0, 500), "11", Color.BLACK, "aaaa");
        }
        hair.setSize(500, 500);
        hair.setVisible(true);

        Bank bank = new Bank();
        Payment payment = new BankConnection(2);
        payment.linkWith(new EnterPassword(bank));
        bank.setPayment(payment);


        Channel channel = new Channel();
        channel.events.subscribe("Кривое зеркало", new SmsNotificationListener("89045554545"));
        channel.startShow("Кривое зеркало");
    }

    private static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }
}

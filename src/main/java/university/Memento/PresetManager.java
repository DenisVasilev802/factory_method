package university.Memento;

public class PresetManager {
    private Preset preset;

    public Preset getPreset() {
        return preset;
    }

    public void setPreset(Preset preset) {
        this.preset = preset;
    }
}

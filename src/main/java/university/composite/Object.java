package university.composite;

public interface Object {
    double getWeight();
}

package university.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Planet implements Object {
    private List<Object> list = new ArrayList<>();
    private double planetWeight;

    public Planet(double planetWeight) {
        this.planetWeight = planetWeight;
    }
    public void add(Object prod)
    {
        list.add(prod);
    }
    public void remove(Object prod)
    {
        if(list.size()>0)
            list.remove(prod);
    }

    @Override
    public double getWeight() {
        return list.stream().map(Object::getWeight).reduce(Double::sum).get()+ planetWeight;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Planet planet = (Planet) o;
        return Double.compare(planet.planetWeight, planetWeight) == 0 &&
                Objects.equals(list, planet.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(list, planetWeight);
    }
}

package university.composite;

import java.util.Objects;

public class Meteor implements Object {
    private String meteorName;
    private double weight;

    public Meteor(String meteorName, double weight) {
        this.meteorName = meteorName;
        this.weight = weight;
    }

    public String getMeteorName() {
        return meteorName;
    }

    public void setMeteorName(String meteorName) {
        this.meteorName = meteorName;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meteor meteor = (Meteor) o;
        return Double.compare(meteor.weight, weight) == 0 &&
                Objects.equals(meteorName, meteor.meteorName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(meteorName, weight);
    }
}

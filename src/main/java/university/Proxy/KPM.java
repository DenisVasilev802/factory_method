package university.Proxy;

/**
 * КПМ – кран портальный монтажный,
 */
public class KPM implements Crane {
    private String id;

    public KPM(String id) {
        this.id = id;
    }

    @Override
    public void rotateTower(double rotate) {
        System.out.println("Rotate tower");
    }

    @Override
    public void rotateCrane(double rotate) {
        System.out.println("Rotate crane");
    }
}

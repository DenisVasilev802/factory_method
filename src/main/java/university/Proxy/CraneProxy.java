package university.Proxy;

public class CraneProxy implements Crane {
    private KPM KPM = null;
    private String id;

    public CraneProxy(String id) {
        this.id = id;
    }
    @Override
    public void rotateTower(double rotate) {
        init();
        KPM.rotateTower(rotate);
    }

    @Override
    public void rotateCrane(double rotate) {
        init();
        KPM.rotateCrane(rotate);
    }

    public void init() {
        if(KPM == null) {
            KPM = new KPM(id);
        }
    }
}

package university.Proxy;

/**
 * Легенда:
 * Управление краном
 */
public interface Crane {
    void rotateTower(double rotate);
    void rotateCrane(double rotate);
}

package university.singleton;

/**
 * Легенда:
 * Звезда на ёлке может быть только 1
 */
public class Star {
    private static Star instance;
    private String treeName;

    public Star() {
    }

    public Star getInstance() {
        {
            if (Star.instance == null) {
                Star.instance = new Star();
            }
            return Star.instance;
        }

    }

    public String getTreeName() {
        return treeName;
    }

    public void setTreeName(String treeName) {
        this.treeName = treeName;
    }
}

package university.chainResponsibility;


public class EnterPassword extends Payment {
    private Bank bank;

    public EnterPassword(Bank bank) {
        this.bank = bank;
    }

    public boolean check(String number, String password) {
        if (!bank.isValidPassword(number,password)) {
            System.out.println("This email is not registered!");
            return false;
        }
        return checkNext(number, password);
    }
}
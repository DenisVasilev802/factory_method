package university.chainResponsibility;

import java.util.HashMap;
import java.util.Map;

public class Bank {
    private Map<String, String> users = new HashMap<>();
    private Payment payment;

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public boolean isValidPassword(String number, String password) {
        return users.get(number).equals(password);
    }
}
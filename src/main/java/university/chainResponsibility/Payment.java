package university.chainResponsibility;

    public abstract class Payment {
        private Payment next;

        public Payment linkWith(Payment next) {
            this.next = next;
            return next;
        }

        public abstract boolean check(String cardNumber, String password);

        protected boolean checkNext(String cardNumber, String password) {
            if (next == null) {
                return true;
            }
            return next.check(cardNumber, password);
        }
    }

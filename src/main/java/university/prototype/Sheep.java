package university.prototype;

import java.util.Objects;

public abstract class Sheep {
    private int age;
    private int weight;
    private String color;

    public Sheep() {
    }

    public Sheep(int age, int weight, String color) {
        this.age = age;
        this.weight = weight;
        this.color = color;
    }

    public Sheep(Sheep target) {
        if (target != null) {
            this.age = target.age;
            this.weight = target.weight;
            this.color = target.color;
        }
    }

    public abstract Sheep clone();

    @Override
    public boolean equals(Object object2) {
        if (!(object2 instanceof Sheep)) return false;
        Sheep shape2 = (Sheep) object2;
        return shape2.age == age && shape2.weight == weight && Objects.equals(shape2.color, color);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Sheep{" +
                "age=" + age +
                ", weight=" + weight +
                ", color='" + color + '\'' +
                '}';
    }
}

package university.prototype;

public class SheepDolly extends Sheep{
    private int numberLegs;

    public SheepDolly() {
    }

    public SheepDolly(int numberLegs,int age, int weight, String color) {
        super(age, weight, color);
        this.numberLegs = numberLegs;
    }

    public SheepDolly(SheepDolly target) {
        super(target);
        if (target != null) {
            this.numberLegs = target.numberLegs;
        }
    }

    @Override
    public Sheep clone() {
        return new SheepDolly(this);
    }

    @Override
    public boolean equals(Object object2) {
        if (!(object2 instanceof SheepDolly) || !super.equals(object2)) return false;
        SheepDolly shape2 = (SheepDolly) object2;
        return shape2.numberLegs == numberLegs;
    }

    public int getNumberLegs() {
        return numberLegs;
    }

    public void setNumberLegs(int numberLegs) {
        this.numberLegs = numberLegs;
    }

    @Override
    public String toString() {
        return "SheepDolly{" +
                "numberLegs=" + numberLegs +
                '}';
    }
}
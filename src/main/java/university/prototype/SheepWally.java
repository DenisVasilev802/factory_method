package university.prototype;

public class SheepWally extends Sheep {

    private int numberEyes;
    private int numberEars;

    public SheepWally() {
    }

    public SheepWally(SheepWally target) {
        super(target);
        if (target != null) {
            this.numberEyes = target.numberEyes;
            this.numberEars = target.numberEars;
        }
    }

    @Override
    public Sheep clone() {
        return new SheepWally(this);
    }

    @Override
    public boolean equals(Object object2) {
        if (!(object2 instanceof SheepWally) || !super.equals(object2)) return false;
        SheepWally shape2 = (SheepWally) object2;
        return shape2.numberEyes == numberEyes && shape2.numberEars == numberEars;
    }

    public int getNumberEyes() {
        return numberEyes;
    }

    public void setNumberEyes(int numberEyes) {
        this.numberEyes = numberEyes;
    }

    public int getNumberEars() {
        return numberEars;
    }

    public void setNumberEars(int numberEars) {
        this.numberEars = numberEars;
    }
}
package university.flyweight;


import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Легенда:
 * Волосы на голове
 */
public class Hair extends JFrame {
    private List<Filament> filaments = new ArrayList<>();

    public void addFilament(int x, int y, String name, Color color, String otherFilamentData) {
        FilamentType type = FilamentFactory.getFilamentType(name, color, otherFilamentData);
        Filament filament = new Filament(x, y, type);
        filaments.add(filament);
    }

    @Override
    public void paint(Graphics graphics) {
        for (Filament filament : filaments) {
            filament.draw(graphics);
        }
    }
}
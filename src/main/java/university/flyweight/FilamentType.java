package university.flyweight;

import java.awt.*;

public class FilamentType {
    private String name;
    private Color color;
    private String otherFilamentData;

    public FilamentType(String name, Color color, String otherFilamentData) {
        this.name = name;
        this.color = color;
        this.otherFilamentData = otherFilamentData;
    }

    public void draw(Graphics g, int x, int y) {
        g.setColor(Color.BLACK);
        g.fillRect(x - 1, y, 3, 5);
        g.setColor(color);
        g.fillOval(x - 5, y - 10, 10, 10);
    }
}

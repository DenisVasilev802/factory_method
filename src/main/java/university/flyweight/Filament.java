package university.flyweight;

import java.awt.*;

public class Filament {
    private int x;
    private int y;
    private FilamentType type;

    public Filament(int x, int y, FilamentType type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }
    public void draw(Graphics g) {
        type.draw(g, x, y);
    }
}
package university.flyweight;


import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class FilamentFactory {
    static Map<String, FilamentType> hairTypes = new HashMap<>();

    public static FilamentType getFilamentType(String name, Color color, String otherTreeData) {
        FilamentType result = hairTypes.get(name);
        if (result == null) {
            result = new FilamentType(name, color, otherTreeData);
            hairTypes.put(name, result);
        }
        return result;
    }
}
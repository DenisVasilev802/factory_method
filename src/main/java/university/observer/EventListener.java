package university.observer;

public interface EventListener {
    void update(String showName);
}

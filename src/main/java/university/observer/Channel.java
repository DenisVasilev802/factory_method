package university.observer;

public class Channel {
    public EventManager events;

    public Channel() {
        this.events = new EventManager("Кривое зеркало", "Новости");
    }

    public void startShow(String showName) {
        events.notify("Кривое зеркало");
    }
}
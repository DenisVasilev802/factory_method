package university.observer;

public class SmsNotificationListener implements EventListener {
    private String phoneNumber;

    public SmsNotificationListener(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    @Override
    public void update(String showName) {
        System.out.println("Sms to " + phoneNumber + ": channel start " + showName);

    }
}
package university.bridge;

/**
 * Легенда:
 * Магическое оружее может быть разное: мечи, посохи луки. Также у них может быть разные способности: замораживание, замедление, воспламенение
 */

public interface Weapon {
    boolean isReadyNow();

    void readyToFight();

    void notReadyToFight();
    String getAbility();
    void setAbility(String ability);

    int getPower();

    void setPower(int percent);

    void talkWithWeapon();
}
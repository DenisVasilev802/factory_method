package university.bridge;

public class Frozen implements Magic {

    protected Weapon weapon;

    public Frozen() {
    }

    public Frozen(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public void SetNewAbility(String ability) {
        System.out.println("Ability: set new");
        weapon.setAbility(ability);
    }

    @Override
    public void SetNewIfReadyToFight(String ability) {
        if (weapon.isReadyNow()) {
            weapon.setAbility(ability);
        }
    }
}
package university.bridge;


public interface Magic {
    void SetNewAbility(String ability);
    void SetNewIfReadyToFight(String ability);
}
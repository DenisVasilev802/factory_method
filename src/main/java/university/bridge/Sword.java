package university.bridge;

public class Sword implements Weapon {
    private boolean isReady = false;
    private int power = 30;
    private String ability = "Frozen";

    @Override
    public boolean isReadyNow() {
        return isReady;
    }

    @Override
    public void readyToFight() {
        isReady = true;
    }

    @Override
    public void notReadyToFight() {
        isReady = false;
    }

    @Override
    public String getAbility() {
        return ability;
    }

    @Override
    public void setAbility(String ability) {
        this.ability = ability;
    }

    @Override
    public int getPower() {
        return power;
    }

    @Override
    public void setPower(int power) {
        this.power=power;
    }

    @Override
    public void talkWithWeapon() {
        System.out.println("------------------------------------");
        System.out.println("| I'm sword.");
        System.out.println("| I'm " + (isReady ? "ready" : "not ready"));
        System.out.println("| Current power is " + power + "%");
        System.out.println("| Current channel is " + ability);
        System.out.println("------------------------------------\n");
    }
}
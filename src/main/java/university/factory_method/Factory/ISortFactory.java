package university.factory_method.Factory;

import university.factory_method.sort_method.ISort;

public interface ISortFactory {
    ISort makeSortMethod();
}

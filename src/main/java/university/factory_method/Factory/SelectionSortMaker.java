package university.factory_method.Factory;

import university.factory_method.sort_method.ISort;
import university.factory_method.sort_method.SelectionSort;

public class SelectionSortMaker implements ISortFactory{
    public ISort makeSortMethod() {
        return new SelectionSort();
    }
}

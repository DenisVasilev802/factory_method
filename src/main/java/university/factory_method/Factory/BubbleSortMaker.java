package university.factory_method.Factory;

import university.factory_method.sort_method.BubbleSort;
import university.factory_method.sort_method.ISort;

public class BubbleSortMaker implements ISortFactory {
    public ISort makeSortMethod() {
        return new BubbleSort();
    }
}

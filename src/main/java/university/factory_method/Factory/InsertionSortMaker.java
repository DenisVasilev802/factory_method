package university.factory_method.Factory;

import university.factory_method.sort_method.ISort;
import university.factory_method.sort_method.InsertionSort;

public class InsertionSortMaker implements ISortFactory {
    public ISort makeSortMethod() {
        return new InsertionSort();
    }
}

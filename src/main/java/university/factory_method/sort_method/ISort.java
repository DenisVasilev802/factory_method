package university.factory_method.sort_method;

public interface ISort {
    /**
     *
     * @param array not sorting array
     * @return sorting array
     */
    int[] sort(int[] array);
}

package university.factory_method.sort_method;

public class SelectionSort implements ISort {
    public int[] sort(int[] array) {
        int buf;
        for (int left = 0; left < array.length; left++) {
            int minInd = left;
            for (int i = left; i < array.length; i++) {
                if (array[i] < array[minInd]) {
                    minInd = i;
                }
            }
            buf = array[left];
            array[left] = array[minInd];
            array[minInd] = buf;
        }
        return array;
    }
}

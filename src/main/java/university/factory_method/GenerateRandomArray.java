package university.factory_method;

public class GenerateRandomArray {
    /**
     * @param from random from number
     * @param to   random to number
     * @param size array size
     * @return
     */
    public int[] generate(int from, int to, int size) {
        int[] mas = new int[size];
        for (int i = 0; i < size; i++) {
            mas[i] = from + (int) (Math.random() * to);
        }
        return mas;
    }
}

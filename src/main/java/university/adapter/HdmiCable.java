package university.adapter;

public class HdmiCable {
    private double size;

    public HdmiCable() {}

    public HdmiCable(double size) {
        this.size = size;
    }

    public double getSize() {
        return size;
    }
}

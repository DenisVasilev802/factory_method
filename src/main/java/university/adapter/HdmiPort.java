package university.adapter;

public class HdmiPort {
    private double size;

    public HdmiPort(double size) {
        this.size = size;
    }

    public double getSize() {
        return size;
    }

    public boolean fits(HdmiCable peg) {
        boolean result;
        result = (this.getSize() >= peg.getSize());
        return result;
    }
}

package university.adapter;

/**
 * Легенда:
 * Переходник с микро hdmi на болшой hdmi
 */
public class HdmiAdapter extends HdmiCable {
    private MiniHdmiCable miniHdmiCable;

    public HdmiAdapter(MiniHdmiCable miniHdmiCable) {
        this.miniHdmiCable = miniHdmiCable;
    }

    @Override
    public double getSize() {
        double result;
        result = miniHdmiCable.getSizeMini();
        return result;
    }
}

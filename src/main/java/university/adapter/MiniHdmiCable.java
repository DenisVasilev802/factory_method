package university.adapter;

public class MiniHdmiCable {
    private double sizeMini;

    public MiniHdmiCable() {}

    public MiniHdmiCable(double sizeMini) {
        this.sizeMini = sizeMini;
    }

    public double getSizeMini() {
        return sizeMini;
    }
}
